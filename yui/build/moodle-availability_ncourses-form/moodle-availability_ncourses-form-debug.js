YUI.add('moodle-availability_ncourses-form', function (Y, NAME) {

/**
 * JavaScript for form editing language conditions.
 *
 * @module moodle-availability_language-form
 */
M.availability_ncourses = M.availability_ncourses || {};

// Class M.availability_language.form @extends M.core_availability.plugin.
M.availability_ncourses.form = Y.Object(M.core_availability.plugin);

// Languages available for selection.
M.availability_ncourses.form.ncourses = null; // si no entiendo mal esto podría ser el número por defecto.

/**
 * Initialises this plugin.
 *
 * @method initInner
 * @param {Array} languages Array of objects containing languageid => name
 */
M.availability_ncourses.form.initInner = function(ncourses) {
    this.ncourses = ncourses;
};

M.availability_ncourses.form.getNode = function(json) {
    // Create HTML structure.
    var tit = M.util.get_string('title', 'availability_ncourses');
    var html = '<label class="form-group"><span class="p-r-1">' + tit + '</span>';
    html += '<span class="availability-ncourses"><select class="custom-select" name="id" title=' + tit + '>';
    html += '<option value="choose">' + M.util.get_string('choosedots', 'moodle') + '</option>';
    var selected = "";
    for ( var i = 0; i < 8; i++ ) {
    	selected = "";
    	if ( i == 5 ) { selected='selected' }
        html += '<option ' + selected + ' value="' + i + '">' + i + '</option>';
    }

/*	for (var i = 0; i < this.ncourses.length; i++) {
        var language = this.ncourses[i];
        html += '<option value="' + ncourses.id + '">' + ncourses.name + '</option>';
    }*/
    html += '</select></span></label>';
    var node = Y.Node.create('<span class="form-inline">' + html + '</span>');

    // Set initial values (leave default 'choose' if creating afresh).
    if (json.creating === undefined) {
        if (json.id !== undefined && node.one('select[name=id] > option[value=' + json.id + ']')) {
            node.one('select[name=id]').set('value', json.id);
        } else if (json.id === undefined) {
            node.one('select[name=id]').set('value', 'choose');
        }
    }

    // Add event handlers (first time only).
    if (!M.availability_ncourses.form.addedEvents) {
        M.availability_ncourses.form.addedEvents = true;
        var root = Y.one('.availability-field');
        root.delegate('change', function() {
            // Just update the form fields.
            M.core_availability.form.update();
        }, '.availability_ncourses select');
    }

    return node;
};

M.availability_ncourses.form.focusAfterAdd = function(node) {
    var selected = node.one('select[name=id]').get('value');
    if (selected === 'choose') {
        // Make default hidden if no value chosen.
        var eyenode = node.ancestor().one('.availability-eye');
        eyenode.simulate('click');
    }
    var target = node.one('input:not([disabled]),select:not([disabled])');
    target.focus();
};

M.availability_ncourses.form.fillValue = function(value, node) {
    var selected = node.one('select[name=id]').get('value');
    if (selected === 'choose') {
        value.id = '';
    } else {
        value.id = selected;
    }
};

M.availability_ncourses.form.fillErrors = function(errors, node) {
    var selected = node.one('select[name=id]').get('value');
    if (selected === 'choose') {
        errors.push('availability_ncourses:missing');
    }
};


}, '@VERSION@', {"requires": ["base", "node", "event", "node-event-simulate", "moodle-core_availability-form"]});
