<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language strings.
 *
 * @package availability_ncourse
 * @copyright 2022 Diego Hernández <dherna@dherna.info>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['description'] = 'Restringe por número de cursos matriculado.';
$string['getdescription'] = 'The student\'s language is {$a}';
$string['getdescriptionnot'] = 'The student\'s language is not {$a}';
$string['missing'] = 'You must select a number of Ncourses.';
$string['pluginname'] = 'Restrición por número de cursos matriculados ';
$string['privacy:metadata'] = 'The availability_ncourses plugin does not store any personal data.';
$string['title'] = 'Restrición por número de cursos';
