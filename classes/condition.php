<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Condition main class.
 *
 * @package availability_ncourse
 * @copyright 2022 Kiko Fernández <kikofdezmun@gmail.com>
 * @copyright 2022 Diego Hernández <dherna@dherna.info>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace availability_ncourses;

/**
 * Condition main class.
 *
 * @package availability_ncourse
 * @copyright 2022 Kiko Fernández <kikofdezmun@gmail.com>
 * @copyright 2022 Diego Hernández <dherna@dherna.info>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class condition extends \core_availability\condition {

    /** @var string number of course enrollment */
    protected $ncourses;


    /**
     * Constructor.
     *
     * @param \stdClass $structure Data structure from JSON decode
     * @throws \coding_exception If invalid data structure.
     */
    public function __construct($structure) {
        // Get ncourse id.
        if (!property_exists($structure, 'id')) {
            $this->ncoursesid = '';
        } else if (is_string($structure->id)) {
            $this->ncoursesid = $structure->id;
        } else {
            throw new \coding_exception('Invalid ->id for ncourses condition');
        }
    }

    /**
     * Saves data back to a structure object.
     *
     * @return \stdClass Structure object
     */
    public function save() {
        $result = (object)['type' => 'ncourses'];
        if ($this->ncoursesid) {
            $result->id = $this->ncoursesid;
        }
        return $result;
    }

    /**
     * Returns a JSON object which corresponds to a condition of this type.
     *
     * Intended for unit testing, as normally the JSON values are constructed
     * by JavaScript code.
     *
     * @param string $ncoursesid 
     * @return stdClass Object representing condition
     */
    public static function get_json($ncoursesid = '') {
        return (object)['type' => 'ncourses', 'id' => $ncoursesid];
    }

    /**
     * Determines whether a particular item is currently available
     * according to this availability condition.
     *
     * @param bool $not Set true if we are inverting the condition
     * @param info $info Item we're checking
     * @param bool $grabthelot Performance hint: if true, caches information
     *   required for all course-modules, to make the front page and similar
     *   pages work more quickly (works only for current user)
     * @param int $userid User ID to check availability for
     * @return bool True if available
     */
    public function is_available($not, \core_availability\info $info, $grabthelot, $userid) {
        global $CFG, $DB, $USER;

        
	       $course = $info->get_course();
        	//echo "<pre><br><br><br><br><br>";
        	//print_r($this);
        	//print_r($course->visible."-> variable de visibilidad <br>");
        	//print_r($this->ncoursesid."->Número de cursos permitidos matricularse<br>");
        	$matriculados = count (array_filter( enrol_get_my_courses(), function($value) use($course) { 
        		if( $value->category == $course->category && $course->visible == 1 ){ 
        			return true; 
        		} 
        		return false; 
        	} ) ) ;
        	//print_r("Estas matriculado en todos estos cursos ".$matriculados."<br>");
        	$allow = false;
        	if ($matriculados < $this->ncoursesid) {
        		print_r("puedes matricularte");
                $allow = true;
        	} else {
                //print_r("NO PUEDES MATRICULARTE YA ESTAS EN ".$matriculados." Cursos");
                $allow = false;
            }
		//echo "</pre>";
                return $allow;
            }

    /**
     * Obtains a string describing this restriction (whether or not
     * it actually applies). Used to obtain information that is displayed to
     * students if the activity is not available to them, and for staff to see
     * what conditions are.
     *
     * @param bool $full Set true if this is the 'full information' view
     * @param bool $not Set true if we are inverting the condition
     * @param info $info Item we're checking
     * @return string Information string (for admin) about all restrictions on this item
     */
    public function get_description($full, $not, \core_availability\info $info) {
        if ($this->ncoursesid != '') {
            $installedncourse = get_string_manager()->get_list_of_translations(false);
            if (array_key_exists($this->ncoursesid, $installedncourse)) {
                $snot = $not ? 'not' : '';
                return get_string('getdescription' . $snot, 'availability_ncourses', $installedncourse[$this->ncourseid]);
            }
        }
        return '';
    }  

    /**
     * Obtains a representation of the options of this condition as a string,
     * for debugging.
     *
     * @return string Text representation of parameters
     */
    protected function get_debug_string() {
        return $this->ncoursesid ?? 'any';
    }
}
