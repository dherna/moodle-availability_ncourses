# Availability ncourses
Restrict module and section access based on user category enrolme. This restriction module complements
AutoEnrol. https://moodle.org/plugins/enrol_autoenrol

# Idea

# Conditional availability conditions
Check the global documentation about conditional availability conditions:
   https://docs.moodle.org/en/Conditional_activities_settings

# Requirements
This plugin requires Moodle 3.9+

# Installation
Install the plugin like any other plugin to folder /availability/condition/ncourses
See http://docs.moodle.org/en/Installing_plugins for details on installing Moodle plugins

# Initial Configuration
This plugin does need configuration after installation of AutoErol. You should enable the restriction in AutoEnrol 
# Theme support
This plugin is developed and tested on Moodle Core's Boost theme and Boost child themes, including Moodle Core's Classic theme.

# Plugin repositories
This plugin will be published and regularly updated on Gitea Disroot: 

# Bug and problem reports / Support requests
#This plugin is carefully developed and thoroughly tested, but bugs and problems can always appear.
#Please report bugs and problems on Github: https://github.com/ewallah/moodle-availability_language/issues
#We will do our best to solve your problems, but please note that due to limited resources we can't always provide per-case support.

# Feature proposals
#Please issue feature proposals on Github: https://github.com/ewallah/moodle-availability_language/issues
#Please create pull requests on Github: https://github.com/ewallah/moodle-availability_language/pulls
We are always interested to read about your feature proposals or even get a pull request from you, but please accept that we can handle your issues only as feature proposals and not as feature requests.

# Moodle release support
This plugin is maintained for the latest major releases of Moodle.

# Status
#[![Build Status](https://github.com/ewallah/moodle-availability_language/workflows/Tests/badge.svg)](https://github.com/ewallah/moodle-availability_language/actions)
#[![Coverage Status](https://coveralls.io/repos/github/ewallah/moodle-availability_language/badge.svg?branch=main)](https://coveralls.io/github/ewallah/moodle-
#availability_language?branch=main)

# Copyright
Diego Hernández
